#include <iostream>
#include <cstdarg>
#include <vector>
#include <algorithm>
#include <string>

std::string sumTwo(std::string term_1, std::string term_2, int base)
{
    int sizeTerm_1 = term_1.size();
    int sizeTerm_2 = term_2.size();

    if (sizeTerm_1 != sizeTerm_2)
    {
        int value = abs(sizeTerm_1 - sizeTerm_2);
        if (sizeTerm_1 > sizeTerm_2)
        {
            for (int i{}; i < value; i++)
            {
                term_2.insert(0, "0");
                sizeTerm_2++;
            }
        }
        else
        {
            for (int i{}; i < value; i++)
            {
                term_1.insert(0, "0");
                sizeTerm_1++;
            }
        }
    }
    std::vector<int> result(sizeTerm_2 + 1);
    int temp_1{}, temp_2{};
    for (int i{sizeTerm_2-1}; i >= 0; i--)
    {
        if (std::isalpha(term_1[i]))
        {
            temp_1 = toupper(term_1[i]) - 'A' + 10;
        }
        else
        {
            temp_1 = term_1[i] - '0';
        }
        if (std::isalpha(term_2[i]))
        {
            temp_2 = toupper(term_2[i]) - 'A' + 10;
        }
        else
        {
            temp_2 = term_2[i] - '0';
        }
        if (temp_1 + temp_2 + result[sizeTerm_2 - i - 1] >= base)
        {   
            result[sizeTerm_2 - i - 1] += temp_1 + temp_2 - base;
            result[sizeTerm_2 - i] = 1;
        }
        else
        {
            result[sizeTerm_2 - i - 1] += temp_1 + temp_2;
        }
    }
    std::string res{};
    std::reverse(result.begin(), result.end());
    int i{};
    while (result[i] == 0)
    {
        i++;
    }
    for(i; i <= sizeTerm_2; i++)
    {
        if (result[i] >= 10)
        {
            res += result[i] + 'A' - 10;
        }
        else
        {
            res += result[i] + '0';
        }
    }
    return res;
}

std::string sum(int base, int countTerm, ...)
{
    va_list list;
    va_start(list, countTerm);

    std::vector<std::string> allTerm(countTerm);

    for (int i{}; i < countTerm; i++)
    {
        allTerm[i] = va_arg(list, std::string);
    }
    va_end(list);
    std::string result{allTerm[0]};
    for(int i{1}; i < countTerm; i++)
    {
        result = sumTwo(result, allTerm[i], base);
    }
    return result;
}

int main()
{
    std::cout << "Student: Ivanchenko Darya\nGroup: M8O-210B-20\n";
    std::string _1 = "AFC";
    std::string _2 = "1239";
    std::string _3 = "F56FC9";

    std::string _4 = sum(16, 3, _1, _2, _3);
    std::cout << "Result: "<< _4 << '\n';
    return 0;
}