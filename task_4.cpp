#include <iostream>
#include <fstream>

int Gorner(std::string num, int base)
{
    int result{};
    for (int i{}; num[i] != '\0'; i++)
    {
        result = result*base + (isdigit(num[i])? num[i] - '0': toupper(num[i]) - 'A' + 10);
    }
    return result;
}

int main(int argc, char *argv[])
{
    std::cout << "Student: Ivanchenko Darya\nGroup: M8O-210B-20\n";
    std::ifstream fin(argv[1]);
    std::ofstream fout;
    fout.open(argv[2]);

    std::string str;
    if (!fin) 
    {
        std::cout << "File don't open\n"; 
        return -1;
    }
    else
    {
        while(!fin.eof())
        {
            getline(fin, str);
            int s{};
            while(str[s])
            {
                int max{};
                std::string number{};
                while(isspace(str[s]))
                    s++;
                while (isalnum(str[s]))
                {
                    number += str[s];
                    s++;
                }
                for (auto elem: number)
                {
                    int temp{};
                    (isalpha(elem))? temp = elem - 'A' + 10: temp = elem - '0';
                    if (temp > max)
                    {
                        max = temp;
                    }
                }
                fout << "Notation is " << max + 1 << ", number is " << Gorner(number, max + 1) << '\n' << std::flush;
            }
        }
        fout.close();
        fin.close();
    }
    return 0;
}