#include <iostream>
#include <cstdarg>
#include <vector>

using namespace std;

typedef struct point
{
    int x;
    int y;
    point(int m_x = 0, int m_y = 0)
    {
        x = m_x;
        y = m_y;
    }
} m_point;

bool is_convex(int countOfPoint, ...)
{
    va_list list;
    va_start(list, countOfPoint);

    vector<m_point> allPoints(countOfPoint);

    for (int i{}; i < countOfPoint; i++)
    {
        allPoints[i] = va_arg(list, m_point);
    }
    va_end(list);

    m_point prev, next;
    prev.x = allPoints[countOfPoint-1].x - allPoints[countOfPoint-2].x;
    prev.y = allPoints[countOfPoint-1].y - allPoints[countOfPoint-2].y;

    next.x = allPoints[0].x - allPoints[countOfPoint - 1].x;
    next.y = allPoints[0].y - allPoints[countOfPoint - 1].y;

    if ((prev.x*next.y - prev.y*next.x) >= 0) // обход идет по часовой стрелке, поэтому поворот направо, значение с -
    {
        return false;
    }
    
    for (int i{1}; i < countOfPoint; i++)
    {
        prev = next;
        next.x = allPoints[i].x - allPoints[i - 1].x;
        next.y = allPoints[i].y  - allPoints[i - 1].y;
        if ((prev.x*next.y - prev.y*next.x) >= 0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    bool result;
    cout << "Student: Ivanchenko Darya\nGroup: M8O-210B-20\n";
    m_point a(1, 1), b(1, 3), c(2, 3), d(2, 2), e(3, 2), f(3, 1);
    result = is_convex(6, a, b, c, d, e, f);
    if (result)
        printf("A polygon is a convex figure\n");
    else
        printf("The polygon is not a convex figure \n");
    result = is_convex(3, a, d, f);
    if (result)
        printf("A polygon is a convex figure\n");
    else
        printf("The polygon is not a convex figure \n");
    return 0;
}