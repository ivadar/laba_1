#include <iostream>
#include <cstdarg>
#include <vector>
#include <fstream>
using namespace std;

int findStr(const char *search, const char *find)
{
    int lenOfFindStr{}, lenOfSearchStr{};
    for (lenOfFindStr; find[lenOfFindStr]; lenOfFindStr++);
    for (lenOfSearchStr; find[lenOfSearchStr]; lenOfSearchStr++);

    int numberOfCoincidences{};
    for (int i{}; search[i]; i++)
    {
        if(search[i] == find[numberOfCoincidences])
        {
            int j{i};
            numberOfCoincidences++;
            while(search[j + 1] == find[numberOfCoincidences] && (j + 1) < lenOfSearchStr && numberOfCoincidences < lenOfFindStr)
            {
                j++; numberOfCoincidences++;
            }
            if (numberOfCoincidences == lenOfFindStr || j == lenOfSearchStr - 1)
                return numberOfCoincidences;
            numberOfCoincidences = 0;
        }
    }
    return numberOfCoincidences;
}

vector<string> find(int (*findPtr)(const char*, const char*), 
                            const char *subStr, int countOfElem, ...)
{
    string file;
    vector<string> fileNames;
    ifstream fin;
    va_list list;
    va_start(list, countOfElem);
    int lenOfSubStr{};
    for (lenOfSubStr; subStr[lenOfSubStr]; lenOfSubStr++);
    for(int i{}; i < countOfElem; i++)
    {
        file = va_arg(list, std::string);
        fin.open(file);
        if(!fin.is_open())
        {
            cout << "The file cannot be opened!\n";
        }
        else
        {
            char *strInput = new char[lenOfSubStr];
            fin.read(strInput, lenOfSubStr);
            int findCount{};
            while(!fin.eof())
            {
                findCount = findPtr(strInput, subStr);             
                if (findCount == lenOfSubStr)
                {
                    fileNames.push_back(file);
                    fin.close();
                    break;
                }
                else if (findCount == 0)
                {
                    fin.read(strInput, lenOfSubStr);  
                    findCount = findPtr(strInput, subStr);
                }
                else
                {
                    fin.seekg(-findCount, ios::cur);
                    fin.read(strInput, lenOfSubStr);  
                }
            }
            delete[] strInput;
            strInput = nullptr;
        }
    }
    va_end(list);
    return fileNames;
}

int main()
{
    cout << "Student: Ivanchenko Darya\nGroup: M8O-210B-20\n";
    char s_str[]{"vodvd"};
    string f = "qwe";

    vector<string> v = find(findStr, s_str, 1, f);
    for (auto el: v)
    {
        cout << "Files: " << el << ' ';
    }
    cout << '\n';
    return 0;
}